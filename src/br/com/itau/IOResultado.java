package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IOResultado {
    private Resultado resultado;

    public IOResultado(Resultado resultado) {
        this.resultado = resultado;
    }

    public Resultado getResultado() {
        return resultado;
    }

    public void setResultado(Resultado resultado) {
        this.resultado = resultado;
    }

    public static String exibirResultadoFinal(Integer[] valoresSoteados, Integer somaValores) {

        String retornoFinal = "";

        for (int i = 0; i < valoresSoteados.length; i++) {
            retornoFinal += valoresSoteados[i].toString() + ",";
        }
        retornoFinal += somaValores.toString();

        return retornoFinal;
    }
}
