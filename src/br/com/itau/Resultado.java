package br.com.itau;

public class Resultado {
    private Sorteio sorteio;
    private Integer resultado;

    public Resultado(Sorteio sorteio, Integer resultado) {
        this.sorteio = sorteio;
        this.resultado = resultado;
    }

    public Resultado(){}

    public Sorteio getSorteio() {
        return sorteio;
    }

    public void setSorteio(Sorteio sorteio) {
        this.sorteio = sorteio;
    }

    public Integer getResultado() {
        return resultado;
    }

    public void setResultado(Integer resultado) {
        this.resultado = resultado;
    }

    public void somaNumerosSorteados() {

        Integer[] valores = new Integer[getSorteio().getQuantidadeSorteio()];
        valores = getSorteio().getNumerosSorteados();

        int soma =0;
        for (int i = 0; i < valores.length; i++) {
             soma += valores[i].intValue();
        }

        setResultado(soma);
    }
}
