package br.com.itau;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Resultado resultado = new Resultado();
        resultado.setSorteio(new Sorteio());

        resultado.getSorteio().SortearNumeros();
        resultado.somaNumerosSorteados();

        System.out.println("************* NÚMERO SORTEADO ****************");
        System.out.println(resultado.getSorteio().getNumeroSorteado());

        System.out.println("************* INDIVIDUAL ****************");
        System.out.println(IOResultado.exibirResultadoFinal(resultado.getSorteio().getNumerosSorteados(), resultado.getResultado()));

        System.out.println("************* GRUPOS ****************");
        for (int i = 0; i < 3; i++) {
            resultado.getSorteio().SortearNumeros();
            resultado.somaNumerosSorteados();
            System.out.println(IOResultado.exibirResultadoFinal(resultado.getSorteio().getNumerosSorteados(), resultado.getResultado()));

        }
    }
}
