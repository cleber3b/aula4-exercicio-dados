package br.com.itau;

import javax.swing.*;
import java.util.Random;

public class Sorteio {
    private Integer rangeNumero = 6;
    private Integer quantidadeSorteio = 3;
    //private Integer quantidadeGrupos = 3;
    private Integer[] numerosSorteados;
    private Integer numeroSorteado;

    public Sorteio(Integer rangeNumero, Integer quantidadeSorteio, Integer[] numerosSorteados, Integer numeroSorteado) {
        this.rangeNumero = rangeNumero;
        this.numerosSorteados = numerosSorteados;
        this.quantidadeSorteio = quantidadeSorteio;
        this.numeroSorteado = numeroSorteado;
    }

    public Sorteio() {
    }

    public Integer getRangeNumero() {

        return rangeNumero;
    }

    public void setRangeNumero(Integer rangeNumero) {
        this.rangeNumero = rangeNumero;
    }

    public Integer[] getNumerosSorteados() {
        return numerosSorteados;
    }

    public void setNumerosSorteados(Integer[] numerosSorteados) {
        this.numerosSorteados = numerosSorteados;
    }

    public Integer getQuantidadeSorteio() {
        return quantidadeSorteio;
    }

    public void setQuantidadeSorteio(Integer quantidadeSorteio) {
        this.quantidadeSorteio = quantidadeSorteio;
    }

    public Integer getNumeroSorteado() {
        return numeroSorteado;
    }

    public void setNumeroSorteado(Integer numeroSorteado) {
        this.numeroSorteado = numeroSorteado;
    }

    public void SortearNumeros() {

        Random random = new Random();
        setNumeroSorteado(random.nextInt(getRangeNumero()));

        Integer[] valores = new Integer[getQuantidadeSorteio()];

        for (int i = 0; i < getQuantidadeSorteio(); i++) {
            valores[i] = random.nextInt(getRangeNumero());
        }

        setNumerosSorteados(valores);
    }
}
